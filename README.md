**Okanya Emmanuel's Portfolio**

Welcome to my personal portfolio! This website showcases my journey as a Computer Science student, highlighting my skills, projects, and work experiences. Explore my projects, learn more about my background, and feel free to get in touch!

**Table of Contents**

About Me

Projects

Skills

Education

Work Experience

Contact

About Me

I am a passionate and motivated Computer Science student from Makerere University, Kampala, Uganda. My expertise lies in web development, software engineering, and problem-solving. I am always excited about learning new technologies and working on innovative projects that make an impact.

**Projects**

**E-commerce Website**

An e-commerce platform where users can register as business owners and upload merchandise for sale.
Technologies used: Python, Django, MySQL, JavaScript.

Check out more of my work in the Projects section.

**Skills**

**Programming Languages**: Python, Java, C++, JavaScript, HTML/CSS

**Frameworks/Technologies**: React, Node.js, Django

**Databases**: MySQL, MongoDB

**Tools**: Git, Docker

**Version Control**: Git, GitHub, GitLab

**Education**

**Makerere University**

Bachelor of Science in Computer Science

2020 - 2025

**St. Peter’s S.S.S Nsambya**

UACE (Uganda Advanced Certificate of Education)

2017 - 2018

**Namilyango College**

UCE (Uganda Certificate of Education)

2012 - 2016

**Work Experience**

**Software Development Intern**

_Stratcom Communications and Computer Solutions Limited_

_June 2023 – August 2023_

Developed frontend features of a fees management system using ReactJS.
Worked with MongoDB and Python on the backend for data handling and logic.
Contact
If you'd like to collaborate on a project, hire me, or just want to get in touch, feel free to reach out!

**Email**: okanyaemmanuel6@gmail.com
**LinkedIn**: https://www.linkedin.com/feed/?trk=guest_homepage-basic_nav-header-signin
**Phone**: +256 771 352 368